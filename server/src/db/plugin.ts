import { injectable } from "inversify";
import { isNil } from "lodash";
import { UserError } from "../errors";
import { Document } from "../graphql/models/document";
import { Plugin } from "../graphql/models/plugin";
import mongoose = require('mongoose');

@injectable()
export class PluginService {
  allPlugins(): Promise<mongoose.Document[]> {
    return Plugin.find().exec();
  }

  plugin(name: string): Promise<mongoose.Document|null> {
    return Plugin.findOne({ name }).exec();
  }

  async state(name: string, documentId: string): Promise<string|null> {
    if (name.match(/^\w+$/) === null) {
      throw new UserError('Invalid plugin name');
    }

    const plugin = await Plugin.findOne({ name });
    if (isNil(plugin)) {
      throw new UserError('Plugin not found');
    }

    const statePath = `pluginStates.${name}`;

    const doc = await Document.findOne({
      _id: documentId,
      [statePath]: { $exists: true }
    });
    if (isNil(doc)) {
      return plugin.get('defaultState');
    }

    return doc.get(statePath) || plugin.get('defaultState');
  }

  async updateState(name: string, input: UpdatePluginStateInput): Promise<void> {
    if (input.protocol === 'internal') {
      throw new UserError('Internal document dem cyan mutate');
    }

    const res = await Document.updateOne({ _id: input.path }, {
      $set: {
        [`pluginStates.${name}`]: input.state
      }
    }).exec();

    if (res.n !== 1) {
      throw new UserError('Document not found');
    }
  }
}

export interface UpdatePluginStateInput {
  protocol: string;
  path: string;
  state: string;
}
