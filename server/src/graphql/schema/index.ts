import { GraphQLSchema } from 'graphql';
import { makeExecutableSchema } from 'graphql-tools';
import { injectable, multiInject } from 'inversify';
import { merge } from 'lodash';
import { SCHEMA_PROVIDER } from '../../constants/types';
import { ISchemaProvider } from '../../interfaces/schema-provider';

@injectable()
export class SchemaProvider {
  constructor(
    @multiInject(SCHEMA_PROVIDER) schemaProviders: ISchemaProvider[]
  ) {
    const rootTypeDefs = [`
      type Query
      type Mutation

      schema {
        query: Query
        mutation: Mutation
      }
    `];

    const rootResolvers = {};

    const typeDefs = ([] as string[]).concat(rootTypeDefs, ...schemaProviders.map(provider => provider.get().typeDefs));
    const resolvers = merge(rootResolvers, ...schemaProviders.map(provider => provider.get().resolvers));

    this.schema = makeExecutableSchema({
      typeDefs,
      resolvers
    });
  }

  private readonly schema: GraphQLSchema;

  get(): GraphQLSchema {
    return this.schema;
  }
}
