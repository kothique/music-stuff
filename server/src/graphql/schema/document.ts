import mongoose = require('mongoose');
import { injectable } from 'inversify';
import { isNil } from 'lodash';
import { ObjectId } from 'mongodb';
import { DocumentFormat, DocumentProtocol } from '../../../../common/documents';
import { PluginService } from '../../db/plugin';
import { UserError } from '../../errors';
import { ISchemaProvider } from '../../interfaces/schema-provider';
import { Document } from '../models/document';

@injectable()
export class DocumentSchemaProvider implements ISchemaProvider {
  constructor(
    private pluginService: PluginService
  ) { }

  private readonly data = {
    typeDefs: [`
      enum DocumentProtocol {
        internal
        collection
      }

      enum DocumentFormatEnum {
        html
        vextab
        musicxml
      }

      input CreateDocumentInput {
        format: String!
        title: String!
        content: String
      }

      input UpdateDocumentInput {
        format: String
        title: String
        content: String
      }

      type DocumentPluginStates {
        metronome: String
      }

      type Document @cacheControl(maxAge: 10) {
        _id: ID!
        title: String!
        format: DocumentFormatEnum!
        content: String
        pluginStates: DocumentPluginStates
        updatedAt: Float!
        createdAt: Float!
      }

      extend type Query {
        allDocuments: [Document!]!
        document(protocol: DocumentProtocol!, path: String!): Document
      }

      extend type Mutation {
        createDocument(input: CreateDocumentInput!): Document
        updateDocument(path: String!, input: UpdateDocumentInput!): Document
        deleteDocument(path: String!): Boolean
      }
    `],

    resolvers: {
      Query: {
        async allDocuments(obj, args, context, info): Promise<mongoose.Document[]> {
          // TODO: move logic to a dedicated service

          const docs = await Document.find().exec();

          return docs;
        },
        async document(obj, args, context, info): Promise<mongoose.Document|null> {
          // TODO: move logic to a dedicated service

          const protocol: DocumentProtocol = args.protocol;
          const path: string = args.path;

          if (protocol === 'collection') {
            const doc = await Document.findOne({ _id: path });

            return doc;
          }

          return null;
        }
      },
      Document: {
        pluginStates: (obj: mongoose.Document) => {
          const states = {
            metronome: this.pluginService.state('metronome', obj._id)
          };

          return states;
        }
      },
      Mutation: {
        async createDocument(obj, args, context, info): Promise<mongoose.Document> {
          // TODO: move logic to a dedicated service

          const format: DocumentFormat = args.input.format;
          const title: string = args.input.title;
          const content: string = isNil(args.input.content) ? '' : args.input.content;

          const otherDoc = await Document.findOne({ title });

          if (!isNil(otherDoc)) {
            throw new UserError(`Title must be unique among all documents`);
          }

          const doc = await new Document({
            protocol: 'collection',
            format,
            title,
            content
          }).save();

          return doc;
        },
        
        async updateDocument(obj, args, context, info): Promise<mongoose.Document> {
          // TODO: move logic to a dedicated service

          const path: string = args.path;

          const doc = await Document.findOne({ _id: path });

          if (isNil(doc)) {
            throw new UserError(`Document not found`);
          }

          const format: DocumentFormat|undefined = args.input.format;
          const title: string|undefined = args.input.title;
          const content: string|undefined = args.input.content;

          if (!isNil(title)) {
            const otherDoc = await Document.findOne({ title });

            if (!isNil(otherDoc) && !(otherDoc._id as ObjectId).equals(doc._id)) {
              throw new UserError(`Title must be unique among all documents`);
            }
          }

          if (!isNil(format)) { doc.set({ format }); }
          if (!isNil(title)) { doc.set({ title }); }
          if (!isNil(content)) { doc.set({ content }); }

          return await doc.save();
        },

        async deleteDocument(obj, args, context, info): Promise<true> {
          // TODO: move logic to a dedicated service

          const path: string = args.path;

          const doc = await Document.findOne({ _id: path });

          if (isNil(doc)) {
            throw new UserError(`Document not found`);
          }

          await doc.remove();

          return true;
        }
      }
    }
  };

  get(): DocumentSchemaProvider['data'] {
    return this.data;
  }
}