import mongoose = require('mongoose');
import { injectable } from 'inversify';
import { PluginService, UpdatePluginStateInput } from '../../db/plugin';
import { ISchemaProvider } from '../../interfaces/schema-provider';

@injectable()
export class PluginSchemaProvider implements ISchemaProvider {
  constructor(
    private pluginService: PluginService
  ) { }

  private readonly data = {
    typeDefs: [`
      input UpdatePluginStateInput {
        protocol: String!
        path: String!
        state: String!
      }

      type Plugin {
        _id: ID!
        name: String!
        defaultState: String
      }

      extend type Query {
        allPlugins: [Plugin!]!
        plugin(name: String!): Plugin
      }

      extend type Mutation {
        updatePluginState(name: String!, input: UpdatePluginStateInput!): Boolean
      }
    `],

    resolvers: {
      Query: {
        allPlugins: (): Promise<mongoose.Document[]> => this.pluginService.allPlugins(),
        plugin: (_obj, args: { name: string }): Promise<mongoose.Document|null> =>
          this.pluginService.plugin(args.name),
      },
      Mutation: {
        updatePluginState: (_obj, args: { name: string, input: UpdatePluginStateInput }): Promise<boolean> =>
          this.pluginService.updateState(args.name, {
            protocol: args.input.protocol,
            path: args.input.path,
            state: args.input.state
          }).then(() => true)
      }
    }
  };

  get(): PluginSchemaProvider['data'] {
    return this.data;
  }
}
