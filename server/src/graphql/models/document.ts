import mongoose = require('mongoose');
import { DOCUMENT_FORMATS } from '../../../../common/documents';
import { createdAtPlugin } from '../../util/created-at-plugin';
import { lastModifiedPlugin } from '../../util/last-modified-plugin';

const documentSchema = new mongoose.Schema({
  title: {
    type: String,
    index: true,
    unique: true,
    required: true
  },
  format: {
    type: String,
    enum: DOCUMENT_FORMATS,
    index: true,
    required: true
  },
  content: {
    type: String
  },
  pluginStates: {
    type: Object,
    properties: {
      metronome: {
        type: String
      }
    }
  }
}, {
  id: false
});

documentSchema.plugin(createdAtPlugin, { name: 'createdAt' });
documentSchema.plugin(lastModifiedPlugin, { name: 'updatedAt', index: true });

export const Document = mongoose.model('Document', documentSchema);
