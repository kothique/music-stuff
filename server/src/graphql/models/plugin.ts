import mongoose = require('mongoose');

const pluginSchema = new mongoose.Schema({
  name: {
    type: String,
    index: true,
    unique: true,
    required: true
  },
  defaultState: {
    type: String
  }
}, {
  id: false
});

export const Plugin = mongoose.model('plugin', pluginSchema);
