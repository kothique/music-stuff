export class InternalError extends Error { }
export class UserError extends Error { }

export function handleUnexpectedError(err: Error): never {
  console.error('Unexpected error: ', err.stack || err);
  return process.exit(1);
}
