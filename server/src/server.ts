import "reflect-metadata"; // must be first
import { Container } from 'inversify';
import { isNil } from "lodash";
import { CONFIG, SCHEMA_PROVIDER } from "./constants/types";
import { handleUnexpectedError } from "./errors";
import { DocumentSchemaProvider } from "./graphql/schema/document";
import { PluginSchemaProvider } from "./graphql/schema/plugin";
import { IServerConf } from "./interfaces/server-conf";
import { ExpressLoader } from "./loaders/express";
import { MongoLoader } from "./loaders/mongo";
import { loadJSON } from "./util/json";

async function bootstrap(): Promise<void> {
  const configFilename = process.argv[2];

  if (isNil(configFilename)) {
    console.error(`You must specify configuration file as the first command line argument.`);
    process.exit(1);
  }

  const container = new Container({
    defaultScope: 'Singleton',
    autoBindInjectable: true
  });

  container.bind<DocumentSchemaProvider>(SCHEMA_PROVIDER).to(DocumentSchemaProvider).inSingletonScope();
  container.bind<PluginSchemaProvider>(SCHEMA_PROVIDER).to(PluginSchemaProvider).inSingletonScope();

  const serverConf = loadJSON<IServerConf>(configFilename);
  container.bind(CONFIG).toConstantValue(serverConf);

  const mongoLoader = container.get(MongoLoader);
  await mongoLoader.load();

  const expressLoader = container.get(ExpressLoader);
  await expressLoader.load();
}

bootstrap()
  .then(() => console.log(`Server successfully started!`))
  .catch(handleUnexpectedError);