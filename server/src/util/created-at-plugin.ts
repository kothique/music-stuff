import mongoose = require('mongoose');
import { ObjectId } from 'bson';
import { isNil } from 'lodash';

const DEFAULT_NAME = 'createdAt';

interface IOptions {
  name?: string;
}

export function createdAtPlugin(schema: mongoose.Schema, options: IOptions = {}) {
  const name: string = isNil(options.name) ? DEFAULT_NAME : options.name;

  schema.virtual(name).get(function (this: any): number {
    const _id: ObjectId = this._id;

    return _id.getTimestamp().valueOf();
  });
}