import mongoose = require('mongoose');
import { get, isNil } from 'lodash';

const DEFAULT_NAME = 'lastModified';

interface IOptions {
  name?: string;
  index?: any;
}

export function lastModifiedPlugin(schema: mongoose.Schema, options: IOptions = {}) {
  const name: string = isNil(options.name) ? DEFAULT_NAME : options.name;
  const index: any|null = get(options, 'index', null);

  schema.add({ [name]: Number });

  if (!isNil(index)) {
    schema.path(name).index(options.index);
  }

  schema.pre('save', function (this: any, next) {
    if (this.isModified()) {
      this[name] = Date.now();
    }
    next();
  });
}