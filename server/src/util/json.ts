import { readFileSync } from "fs";

export function loadJSON<T = any>(filename: string): T|never {
  try {
    const text: string = readFileSync(filename, 'utf8');
    return JSON.parse(text);
  } catch (err) {
    console.error(`Error while parsing JSON: ${err.stack || err}`);
    return process.exit(1);
  }
}
