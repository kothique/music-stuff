export interface IServerConf {
  server: {
    host: string;
    port: number;
  };

  mongo: {
    database: string;
    host: string;
    port: number;
    username?: string;
    password?: string;
  };
}