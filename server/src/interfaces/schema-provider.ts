export interface ISchemaProvider {
  get(): {
    typeDefs: string[];
    resolvers: {[field: string]: any};
  };
}