import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import { Request } from 'express';
import { inject, injectable } from "inversify";
import { CONFIG } from '../constants/types';
import { SchemaProvider } from '../graphql/schema';
import { IServerConf } from '../interfaces/server-conf';
import express = require('express');
import compression = require('compression');
import cors = require('cors');

@injectable()
export class ExpressLoader {
  constructor(
    @inject(CONFIG) private config: IServerConf,
    @inject(SchemaProvider) private schemaProvider: SchemaProvider
  ) { }

  async load(): Promise<void> {
    const app = express();

    app.use(cors());
    app.use(compression());
    app.use(express.json());

    app.use('/graphql', graphqlExpress((req: Request) => ({
      schema: this.schemaProvider.get(),
      tracing: true,
      cacheControl: true,
      debug: true,
      context: { }
    })));

    app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

    app.listen(this.config.server.port, () => {
      const { host, port } = this.config.server;

      console.log(`Server is now running on ${port}.`);
      console.log(`GraphiQL is now running on http://${host}:${port}/graphiql.`);
    });
  }
}