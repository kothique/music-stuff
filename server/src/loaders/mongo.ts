import { inject, injectable } from 'inversify';
import { isNil } from 'lodash';
import { Db } from 'mongodb';
import { CONFIG } from '../constants/types';
import { IServerConf } from '../interfaces/server-conf';
import mongoose = require('mongoose');

@injectable()
export class MongoLoader {
  constructor(
    @inject(CONFIG) private config: IServerConf
  ) { }

  async load(): Promise<Db> {
    const mongo = this.config.mongo;
    const uri = isNil(mongo.username) || isNil(mongo.password)
      ? `mongodb://${mongo.host}:${mongo.port}/${mongo.database}`
      : (() => {
        const username = encodeURIComponent(mongo.username);
        const password = encodeURIComponent(mongo.password);

        return `mongodb://${username}:${password}@${mongo.host}:${mongo.port}/${mongo.database}`;
      })();

    mongoose.Promise = global.Promise;
    mongoose.connect(uri, { useNewUrlParser: true });

    return new Promise<Db>((resolve, reject) => {
      mongoose.connection.on('open', () => {
        console.log(`Successfully connected to MongoDB.`);
        resolve();
      });
      mongoose.connection.on('error', err => {
        console.log(`Failed to connect to MongoDB.`);
        reject(err);
      });
    });
  }
}
