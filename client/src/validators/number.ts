import { FormControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export const numberValidator: ValidatorFn = (control: FormControl): ValidationErrors|null => {
  return isNaN(+control.value) ? { number: true } : null;
};
