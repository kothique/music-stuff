export interface SelectOption<T extends string = string> {
  text: string;
  value: T;
}
