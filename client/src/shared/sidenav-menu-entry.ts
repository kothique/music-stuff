import { DocumentLink } from '../app/content/document-link';

export interface SidenavMenuEntry {
  title: string;
  docLink: DocumentLink;
}
