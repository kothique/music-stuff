import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TabsService } from '../content/tabs.service';

@Component({
  selector: 'app-toolbar',
  template: `
    <app-toolbar-dumb
      [isSmall]="isSmall$ | async"
      [drawerOpened]="drawerOpened"
      [rightDrawerOpened]="rightDrawerOpened"
      (toggleDrawer)="toggleDrawerEmitter.emit()"
      (toggleRightDrawer)="toggleRightDrawerEmitter.emit()"
      (create)="onCreate()">
    </app-toolbar-dumb>
  `
})
export class ToolbarComponent implements OnInit {
  constructor(
    private breakpointObserver: BreakpointObserver,
    private tabsService: TabsService
  ) { }

  @Input() drawerOpened: boolean;
  @Input() rightDrawerOpened: boolean;
  @Output('toggleDrawer') toggleDrawerEmitter = new EventEmitter<void>();
  @Output('toggleRightDrawer') toggleRightDrawerEmitter = new EventEmitter<void>();

  isSmall$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  ngOnInit(): void { }

  onCreate(): void {
    this.tabsService.create();
  }
}
