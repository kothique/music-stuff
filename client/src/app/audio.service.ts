import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forOwn, isNil } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  constructor(
    private http: HttpClient
  ) {
    this.loadBuffers();
  }

  private context = new AudioContext;
  private data: {[id: string]: Sound} = {
    [Sounds.Metronome]: { url: '/assets/audio/metronome.wav' }
  };

  playSound(id: Sounds, time: number = 0): void {
    const sound: Sound|undefined = this.data[id];
    if (isNil(sound)) {
      console.warn(`Failed to play sound: invalid sound id`);
      return;
    }

    if (isNil(sound.buffer)) { return; }

    const source = this.context.createBufferSource();
    source.buffer = sound.buffer;
    source.connect(this.context.destination);
    source.start(this.context.currentTime + time);
  }

  private loadBuffers(): void {
    forOwn(this.data, async sound => sound.buffer = await this.loadBuffer(sound.url));
  }

  private async loadBuffer(url): Promise<AudioBuffer|null> {
    let data: ArrayBuffer;

    try {
      data = await this.http.get(url, {
        responseType: 'arraybuffer'
      }).toPromise();
    } catch (err) {
      console.error(`Failed to load audio (${url}): `, err.stack || err);
      return null;
    }

    try {
      const buffer = await this.context.decodeAudioData(data);
      if (isNil(buffer)) {
        console.error(`Failed to decode audio (${url}).`);
        return null;
      }

      return buffer;
    } catch (err) {
      console.error(`Failed to decode audio (${url}): `, err.stack || err);
    }
  }
}

export const enum Sounds {
  Metronome = 'metronome'
}

interface Sound {
  url: string;
  buffer?: AudioBuffer;
}
