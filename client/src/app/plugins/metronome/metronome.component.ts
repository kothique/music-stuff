import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { debounce, isNil } from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { numberValidator } from '../../../validators/number';
import { AudioService, Sounds } from '../../audio.service';
import { BufferService } from '../../content/buffer.service';
import { Document } from '../../content/document';
import { DocumentLink } from '../../content/document-link';
import { PluginsService } from '../../plugins.service';

@Component({
  selector: 'app-metronome',
  templateUrl: './metronome.component.html',
  styleUrls: ['./metronome.component.css']
})
export class MetronomeComponent implements OnInit, OnChanges, OnDestroy {
  constructor(
    private audioService: AudioService,
    private pluginsService: PluginsService,
    private bufferService: BufferService
  ) { }

  @Input() docLink: DocumentLink;

  doc: Document|null = null;
  playing = false;
  tempo = new FormControl('60', [Validators.required, numberValidator]);

  private intervalId: NodeJS.Timer|null = null;
  private unsubscribe$ = new Subject<void>();

  ngOnInit(): void {
    this.tempo.valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        if (this.playing === true) {
          this.start();
        }

        this.saveState();
      });
  }

  private start(): void {
    if (this.intervalId !== null) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }

    const interval = 60 * 1000 / Number(this.tempo.value);

    if (isFinite(interval) === false || interval < 100) {
      this.playing = false;
      return;
    }

    this.audioService.playSound(Sounds.Metronome);
    this.intervalId = setInterval(() => {
      this.audioService.playSound(Sounds.Metronome);
    }, interval);

    this.playing = true;
  }

  private stop(): void {
    if (this.intervalId !== null) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }

    this.playing = false;
  }

  onPlayPause(): void {
    this.playing === true ? this.stop() : this.start();
  }

  // tslint:disable-next-line:member-ordering
  private saveState = debounce(async (): Promise<void> => {
    const tempo: string = this.tempo.value;
    const state = tempo;

    await this.pluginsService.updateState('metronome', this.docLink, state, this.doc._id);
  }, 300);

  private async loadState(): Promise<void> {
    const state = await this.pluginsService.getState('metronome', this.docLink);
    const tempo = state;

    this.tempo.setValue(tempo, { emitEvent: false });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes.docLink)) {
      const docLink: DocumentLink = changes.docLink.currentValue;
      this.doc = this.bufferService.get(docLink);

      this.loadState().then(() => {
        if (this.playing === true) {
          this.start();
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
