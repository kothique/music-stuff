import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { MetronomeComponent } from './metronome/metronome.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [
    MetronomeComponent
  ],
  declarations: [
    MetronomeComponent,
  ]
})
export class PluginsModule { }
