import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  constructor(
    private breakpointObserver: BreakpointObserver
  ) { }

  isSmall$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Small, Breakpoints.XSmall])
    .pipe(map(res => res.matches));

  isLarge$: Observable<boolean> = this.breakpointObserver
    .observe('(min-width: 1500px)')
    .pipe(map(res => res.matches));
}
