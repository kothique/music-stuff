import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { AppComponent } from './app.component';
import { ContentModule } from './content/content.module';
import { LayoutComponent } from './layout/layout.component';
import { MaterialModule } from './material/material.module';
import { PipesModule } from './pipes/pipes.module';
import { ToolbarDumbComponent } from './toolbar-dumb/toolbar-dumb.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

const GRAPHQL_ENDPOINT = 'http://localhost:8080/graphql';

export function createApollo(httpLink: HttpLink) {
  return {
    link: httpLink.create({ uri: GRAPHQL_ENDPOINT }),
    cache: new InMemoryCache()
    //   dataIdFromObject: (object: any) => object._id
    // })
  };
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    ToolbarComponent,
    ToolbarDumbComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    HttpClientModule,
    ApolloModule,
    HttpLinkModule,
    BrowserAnimationsModule,
    MaterialModule,
    ContentModule,
    PipesModule
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
