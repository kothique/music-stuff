import { Component, OnInit } from '@angular/core';
import { AudioService, Sounds } from './audio.service';

@Component({
  selector: 'app-root',
  template: `
    <app-layout></app-layout>
  `
})
export class AppComponent implements OnInit {
  constructor() { }

  ngOnInit(): void { }
}
