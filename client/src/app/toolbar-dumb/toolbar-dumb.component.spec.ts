import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarDumbComponent } from './toolbar-dumb.component';

describe('ToolbarDumbComponent', () => {
  let component: ToolbarDumbComponent;
  let fixture: ComponentFixture<ToolbarDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
