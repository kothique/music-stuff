import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar-dumb',
  templateUrl: './toolbar-dumb.component.html',
  styleUrls: ['./toolbar-dumb.component.css']
})
export class ToolbarDumbComponent implements OnInit {
  constructor() { }

  @Input() isSmall: boolean;
  @Input() drawerOpened: boolean;
  @Input() rightDrawerOpened: boolean;
  @Output('create') createEmitter = new EventEmitter<void>();
  @Output('toggleDrawer') toggleDrawerEmitter = new EventEmitter<void>();
  @Output('toggleRightDrawer') toggleRightDrawerEmitter = new EventEmitter<void>();

  ngOnInit(): void { }
}
