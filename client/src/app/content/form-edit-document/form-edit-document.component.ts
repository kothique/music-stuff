import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { DocumentFormat } from '../../../../../common/documents';
import { SelectOption } from '../../../shared/select-option';
import { Document, DocumentInput } from '../document';

@Component({
  selector: 'app-form-edit-document',
  templateUrl: './form-edit-document.component.html',
  styleUrls: ['./form-edit-document.component.css']
})
export class FormEditDocumentComponent implements OnInit, OnDestroy {
  constructor(
    private formBuilder: FormBuilder
  ) { }

  @Input() doc: Document;
  @Input() docChanges: DocumentInput;
  @Output() docChangesChange = new EventEmitter<DocumentInput>();

  readonly formats: SelectOption<DocumentFormat>[] = [
    { text: 'HTML', value: 'html' },
    { text: 'VexTab', value: 'vextab' },
    { text: 'MusicXML', value: 'musicxml' }
  ];
  readonly defaultFormat = this.formats[0];

  form: FormGroup = this.formBuilder.group({
    format: [this.defaultFormat.value, Validators.required],
    title: ['', Validators.required],
    content: ''
  });

  private unsubscribe$ = new Subject<void>();

  ngOnInit(): void {
    this.form.valueChanges.subscribe(value => {
      this.docChangesChange.emit(value);
    });

    this.form.setValue({
      format: this.doc.format,
      title: this.doc.title,
      content: this.doc.content
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
