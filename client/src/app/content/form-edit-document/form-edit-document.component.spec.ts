import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEditDocumentComponent } from './form-edit-document.component';

describe('FormEditDocumentComponent', () => {
  let component: FormEditDocumentComponent;
  let fixture: ComponentFixture<FormEditDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEditDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEditDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
