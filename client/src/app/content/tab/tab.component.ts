import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Tab } from '../tab';
import { TabsService } from '../tabs.service';

@Component({
  selector: 'app-tab',
  template: `
    <app-tab-dumb [tab]="tab$ | async">
    </app-tab-dumb>
  `
})
export class TabComponent implements OnInit {
  constructor(
    private tabsService: TabsService
  ) { }

  tab$: Observable<Tab> = this.tabsService.activeTab$;

  ngOnInit(): void { }
}
