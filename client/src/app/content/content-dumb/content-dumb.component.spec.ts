import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentDumbComponent } from './content-dumb.component';

describe('ContentDumbComponent', () => {
  let component: ContentDumbComponent;
  let fixture: ComponentFixture<ContentDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
