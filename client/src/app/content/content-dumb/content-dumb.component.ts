import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { isNil } from 'lodash';
import { Tab } from '../tab';

@Component({
  selector: 'app-content-dumb',
  templateUrl: './content-dumb.component.html',
  styleUrls: ['./content-dumb.component.css']
})
export class ContentDumbComponent implements OnInit {
  constructor() { }

  @Input() activeTab: Tab|null;
  @Input() activeIndex: number|null;
  @Input() tabs: Tab[];
  @Input() wide: boolean;
  @Input() drawerOpened: boolean;
  @Input() rightDrawerOpened: boolean;
  @Input() shrinkTabs: boolean;
  @Output('open') openEmitter = new EventEmitter<number>();
  @Output('edit') editEmitter = new EventEmitter<number>();
  @Output('save') saveEmitter = new EventEmitter<number>();
  @Output('delete') deleteEmitter = new EventEmitter<number>();
  @Output('cancel') cancelEmitter = new EventEmitter<number>();
  @Output('close') closeEmitter = new EventEmitter<number>();

  ngOnInit(): void { }

  getTitle(tab: Tab|null): string|null {
    if (isNil(tab)) { return null; }

    if (tab.state.type === 'normal' || tab.state.type === 'editing') {
      return tab.state.doc.title;
    } else {
      return 'New document';
    }
  }

  onOpen(index: number): void {
    this.openEmitter.emit(index);
  }

  onEdit(index: number|null): void {
    if (isNil(index)) { return; }

    this.editEmitter.emit(index);
  }

  onSave(index: number|null): void {
    if (isNil(index)) { return; }

    this.saveEmitter.emit(index);
  }

  onDelete(index: number|null): void {
    if (isNil(index)) { return; }

    this.deleteEmitter.emit(index);
  }

  onCancel(index: number|null): void {
    if (isNil(index)) { return; }

    this.cancelEmitter.emit(index);
  }

  onClose(index: number|null): void {
    if (isNil(index)) { return; }

    this.closeEmitter.emit(index);
  }
}
