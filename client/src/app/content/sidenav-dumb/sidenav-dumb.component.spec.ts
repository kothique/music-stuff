import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavDumbComponent } from './sidenav-dumb.component';

describe('SidenavDumbComponent', () => {
  let component: SidenavDumbComponent;
  let fixture: ComponentFixture<SidenavDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
