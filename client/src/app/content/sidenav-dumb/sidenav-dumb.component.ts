import { Component, Input, OnInit } from '@angular/core';
import { SidenavMenuEntry } from '../../../shared/sidenav-menu-entry';
import { DocumentLink } from '../document-link';
import { DocumentService } from '../document.service';

@Component({
  selector: 'app-sidenav-dumb',
  templateUrl: './sidenav-dumb.component.html',
  styleUrls: ['./sidenav-dumb.component.css']
})
export class SidenavDumbComponent implements OnInit {
  constructor(
    private documentService: DocumentService
  ) { }

  @Input() entries: SidenavMenuEntry[];

  ngOnInit(): void { }

  trackByDocLink(index: number, entry: SidenavMenuEntry): string {
    return entry.docLink.toString();
  }

  onOpen(docLink: DocumentLink): void {
    this.documentService.open(docLink);
  }
}
