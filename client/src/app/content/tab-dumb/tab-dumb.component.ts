import { Component, Input, OnInit } from '@angular/core';
import { Tab } from '../tab';

@Component({
  selector: 'app-tab-dumb',
  templateUrl: './tab-dumb.component.html'
})
export class TabDumbComponent implements OnInit {
  constructor() { }

  @Input() tab: Tab;

  ngOnInit(): void { }
}
