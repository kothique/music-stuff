import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabDumbComponent } from './tab-dumb.component';

describe('TabDumbComponent', () => {
  let component: TabDumbComponent;
  let fixture: ComponentFixture<TabDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
