import { Injectable, OnDestroy } from '@angular/core';
import { isNil } from 'lodash';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Tab, TabState } from 'src/app/content/tab';
import { BufferService } from './buffer.service';
import { Document } from './document';
import { DocumentLink } from './document-link';
import { DocumentService } from './document.service';

@Injectable({
  providedIn: 'root'
})
export class TabsService implements OnDestroy {
  constructor(
    private bufferService: BufferService,
    private documentService: DocumentService
  ) {
    this.documentService.open$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(docLink => {
        const doc = this.bufferService.get(docLink);
        this.insert(docLink, doc, { follow: true });
      });

    this.documentService.update$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(docLink => {
        const tab = this.byDocumentLink[docLink.toString()];
        if (isNil(tab)) { return; }

        const newDoc = this.bufferService.get(docLink);
        if (isNil(newDoc)) { return; }

        const prevState = tab.editing2normal(newDoc);
        this.stateChangeSubject.next({ tab, prevState });
      });

    this.documentService.delete$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(docLink => {
        const tab = this.byDocumentLink[docLink.toString()];
        if (isNil(tab)) { return; }

        const tabs = this.tabs.getValue();
        const index = tabs.indexOf(tab);

        this.closeByIndex(index);
      });

    this.documentService.close$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(docLink => this.closeByDocumentLink(docLink));
  }

  private tabs = new BehaviorSubject<Tab[]>([]);
  readonly tabs$ = this.tabs.asObservable();

  private activeTab = new BehaviorSubject<Tab|null>(null);
  readonly activeTab$ = this.activeTab.asObservable();

  private activeIndex = new BehaviorSubject<number|null>(null);
  readonly activeIndex$ = this.activeIndex.asObservable();

  private openSubject = new Subject<number>();
  readonly open$ = this.openSubject.asObservable();

  private stateChangeSubject = new Subject<StateChangePayload>();
  readonly stateChange$ = this.stateChangeSubject.asObservable();

  private closeSubject = new Subject<number>();
  readonly close$ = this.closeSubject.asObservable();

  private byDocumentLink: {[dlHash: string]: Tab} = {};

  private unsubscribe$ = new Subject<void>();

  openByIndex(index: number): void {
    const tabs = this.tabs.getValue();
    const tab: Tab|undefined = tabs[index];

    if (isNil(tab)) {
      console.warn(`Failed to open tab: invalid index (${index})`);
      return;
    }

    this.activeTab.next(tab);
    this.activeIndex.next(index);
    this.openSubject.next(index);
  }

  editByIndex(index: number): void {
    const tabs = this.tabs.getValue();
    const tab: Tab|undefined = tabs[index];

    if (isNil(tab)) {
      console.warn(`Failed to edit tab: invalid index (${index})`);
      return;
    }

    const prevState = tab.normal2editing();
    this.stateChangeSubject.next({ tab, prevState });
  }

  async saveByIndex(index: number): Promise<void> {
    const tabs = this.tabs.getValue();
    const tab: Tab|undefined = tabs[index];

    if (isNil(tab)) {
      console.warn(`Failed to save tab: invalid index (${index})`);
      return;
    }

    if (tab.state.type === 'editing') {
      await this.documentService.update(tab.state.docLink, tab.state.input);
    } else if (tab.state.type === 'creating') {
      if (isNil(tab.state.input.format) || isNil(tab.state.input.title) || isNil(tab.state.input.content)) {
        return;
      }

      const docLink = await this.documentService.create({
        format: tab.state.input.format,
        title: tab.state.input.title,
        content: tab.state.input.content
      });
      if (isNil(docLink)) { return; }

      this.byDocumentLink[docLink.toString()] = tab;

      const doc = this.bufferService.get(docLink);
      tab.creating2normal(doc, docLink);
    }
  }

  cancelByIndex(index: number): void {
    const tabs = this.tabs.getValue();
    const tab: Tab|undefined = tabs[index];

    if (isNil(tab)) {
      console.warn(`Failed to cancel editing tab: invalid index (${index})`);
      return;
    }

    if (tab.state.type !== 'editing') { return; }
    const prevState = tab.editing2normal();
    this.stateChangeSubject.next({ tab, prevState });
  }

  closeByIndex(index: number): void {
    const tabs = this.tabs.getValue();
    const tab: Tab|undefined = tabs[index];

    if (isNil(tab)) {
      console.warn(`Failed to close tab: invalid index (${index})`);
      return;
    }

    const newTabs = [...tabs.slice(0, index), ...tabs.slice(index + 1)];

    if (tab.state.type === 'normal' || tab.state.type === 'editing') {
      const dlHash = tab.state.docLink.toString();
      delete this.byDocumentLink[dlHash];
    }

    this.tabs.next(newTabs);
    this.closeSubject.next(index);

    if (newTabs.length === 0) {
      this.activeTab.next(null);
      this.activeIndex.next(null);
    } else if (index > 0) {
      this.openByIndex(index - 1);
    } else if (index === 0) {
      this.openByIndex(index);
    }
  }

  closeByDocumentLink(docLink: DocumentLink): boolean {
    const tabs = this.tabs.getValue();
    const tab = this.byDocumentLink[docLink.toString()];

    if (isNil(tab)) {
      return false;
    }

    const index = tabs.indexOf(tab);
    this.closeByIndex(index);
    return true;
  }

  getTab(index: number): Tab|null {
    const tabs = this.tabs.getValue();
    const tab: Tab|undefined = tabs[index];

    if (isNil(tab)) {
      console.warn(`Failed to get document link: invalid index (${index})`);
      return null;
    }

    return tab;
  }

  insert(docLink: DocumentLink, doc: Document, options: InsertOptions = {}): Tab {
    const follow = isNil(options.follow) ? true : options.follow;

    const dlHash = docLink.toString();
    const tab = this.byDocumentLink[dlHash];

    if (!isNil(tab)) {
      if (follow === true) {
        const index = this.tabs.getValue().indexOf(tab);
        this.openByIndex(index);
      }
      return tab;
    }

    const tabs = this.tabs.getValue();
    const activeTab = this.activeTab.getValue();
    const newTab = Tab.document(doc, docLink);
    const newTabs = [...tabs, newTab];
    const newIndex = newTabs.length - 1;

    this.tabs.next(newTabs);
    this.byDocumentLink[dlHash] = newTab;
    if (isNil(activeTab) || follow === true) {
      this.openByIndex(newIndex);
    }

    return newTab;
  }

  create(options: CreateOptions = {}): Tab {
    const follow = isNil(options.follow) ? true : options.follow;

    const tabs = this.tabs.getValue();
    const activeTab = this.activeTab.getValue();
    const newTab = Tab.empty();
    const newTabs = [...tabs, newTab];
    const newIndex = newTabs.length - 1;

    this.tabs.next(newTabs);
    if (isNil(activeTab) || follow === true) {
      this.openByIndex(newIndex);
    }

    return newTab;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

interface StateChangePayload {
  tab: Tab;
  prevState: TabState;
}

interface InsertOptions {
  follow?: boolean;
}

interface CreateOptions {
  follow?: boolean;
}
