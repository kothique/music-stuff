import { isNil } from 'lodash';
import { Document, DocumentInput } from './document';
import { DocumentLink } from './document-link';

export class Tab {
  private constructor(public state: TabState) { }

  static document(doc: Document, docLink: DocumentLink): Tab {
    return new Tab({ type: 'normal', doc, docLink });
  }

  static empty(): Tab {
    return new Tab({ type: 'creating', input: {}});
  }

  editing2normal(newDoc?: Document): TabState {
    if (this.state.type !== 'editing') {
      this.warnInvalidState('editing');
      return;
    }

    const doc = isNil(newDoc) ? this.state.doc : newDoc;
    const docLink = this.state.docLink;

    const prevState = this.state;
    this.state = { type: 'normal', doc, docLink };

    return prevState;
  }

  creating2normal(doc: Document, docLink: DocumentLink): TabState {
    if (this.state.type !== 'creating') {
      this.warnInvalidState('creating');
      return;
    }

    const prevState = this.state;
    this.state = { type: 'normal', doc, docLink };

    return prevState;
  }

  normal2editing(): TabState {
    if (this.state.type !== 'normal') {
      this.warnInvalidState('normal');
      return;
    }

    const prevState = this.state;
    this.state = {
      type: 'editing',
      doc: this.state.doc,
      docLink: this.state.docLink,
      input: {}
    };

    return prevState;
  }

  private warnInvalidState(expected: string): void {
    console.warn(`Tab state expected to be ${expected}, got: ${this.state}`);
  }
}

export type TabState = TabStateNormal | TabStateEditing | TabStateCreating;

interface TabStateNormal {
  type: 'normal';
  doc: Document;
  docLink: DocumentLink;
}

interface TabStateEditing {
  type: 'editing';
  doc: Document;
  docLink: DocumentLink;
  input: DocumentInput;
}

interface TabStateCreating {
  type: 'creating';
  input: DocumentInput;
}
