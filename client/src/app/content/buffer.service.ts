import { Injectable } from '@angular/core';
import { isNil } from 'lodash';
import { Subject } from 'rxjs';
import { Document } from './document';
import { DocumentLink } from './document-link';

@Injectable({
  providedIn: 'root'
})
export class BufferService {
  constructor() { }

  private docs: {[dl: string]: Document} = {};

  private addSubject = new Subject<DocumentLink>();
  readonly add$ = this.addSubject.asObservable();

  private updateSubject = new Subject<DocumentLink>();
  readonly update$ = this.updateSubject.asObservable();

  private removeSubject = new Subject<DocumentLink>();
  readonly remove$ = this.removeSubject.asObservable();

  get(docLink: DocumentLink): Document|null {
    const dlHash = docLink.toString();
    const doc: Document|undefined = this.docs[dlHash];

    return isNil(doc) ? null : doc;
  }

  add(docLink: DocumentLink, doc: Document): void {
    const dlHash = docLink.toString();

    if (!isNil(this.docs[dlHash])) {
      return;
    }

    this.docs[dlHash] = doc;
    this.addSubject.next(docLink);
  }

  update(docLink: DocumentLink, newDoc: Document): void {
    const dlHash = docLink.toString();

    if (isNil(this.docs[dlHash])) {
      return;
    }

    this.docs[dlHash] = newDoc;
    this.updateSubject.next(docLink);
  }

  remove(docLink: DocumentLink): void {
    const dlHash = docLink.toString();
    const doc = this.docs[dlHash];

    if (isNil(doc)) {
      return;
    }

    delete this.docs[dlHash];
    this.removeSubject.next(docLink);
  }
}
