import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../../material/material.module';
import { HtmlDocumentComponent } from './html-document/html-document.component';
import { MusicXMLDocumentComponent } from './musicxml-document/musicxml-document.component';
import { VextabDocumentComponent } from './vextab-document/vextab-document.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    HtmlDocumentComponent,
    VextabDocumentComponent,
    MusicXMLDocumentComponent
  ],
  declarations: [
    HtmlDocumentComponent,
    VextabDocumentComponent,
    MusicXMLDocumentComponent
  ]
})
export class DocumentsModule { }
