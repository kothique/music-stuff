import { Component, Input, OnInit } from '@angular/core';
import { Document } from '../../document';

@Component({
  selector: 'app-vextab-document',
  templateUrl: './vextab-document.component.html',
  styleUrls: ['./vextab-document.component.css']
})
export class VextabDocumentComponent implements OnInit {
  constructor() { }

  @Input() doc: Document;

  ngOnInit(): void { }
}
