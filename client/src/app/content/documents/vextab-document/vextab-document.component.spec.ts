import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VextabDocumentComponent } from './vextab-document.component';

describe('VextabDocumentComponent', () => {
  let component: VextabDocumentComponent;
  let fixture: ComponentFixture<VextabDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VextabDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VextabDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
