import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { get } from 'lodash';
import { Document } from '../../document';

@Component({
  selector: 'app-html-document',
  template: ''
})
export class HtmlDocumentComponent implements OnInit {
  constructor(
    private sanitizer: DomSanitizer
  ) { }

  @Input() doc: Document;
  @HostBinding('innerHTML') get innerHTML() {
    const html = get(this.doc, 'content');
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  ngOnInit(): void { }
}
