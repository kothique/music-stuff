import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicXMLDocumentComponent } from './musicxml-document.component';

describe('MusicXMLDocumentComponent', () => {
  let component: MusicXMLDocumentComponent;
  let fixture: ComponentFixture<MusicXMLDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicXMLDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicXMLDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
