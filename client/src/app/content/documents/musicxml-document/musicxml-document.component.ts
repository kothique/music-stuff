import { AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { isNil } from 'lodash';
import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Document } from '../../document';

@Component({
  selector: 'app-musicxml-document',
  templateUrl: './musicxml-document.component.html',
  styleUrls: ['./musicxml-document.component.css']
})
export class MusicXMLDocumentComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  constructor() { }

  @Input() doc: Document;
  @ViewChild('container') container;

  error: string|null = null;
  private display: OpenSheetMusicDisplay|null = null;
  private unsubscribe$ = new Subject<void>();

  ngOnInit(): void {
    fromEvent(window, 'resize').pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(500)
    ).subscribe(() => this.render());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.doc.isFirstChange() === true || isNil(changes.doc)) {
      return;
    }

    this.load();
  }

  ngAfterViewInit(): void {
    this.display = new OpenSheetMusicDisplay(this.container.nativeElement);
    this.load();
  }

  private async load(): Promise<void> {
    try {
      await this.display.load(this.doc.content);
      await this.render();
      this.error = null;
    } catch (err) {
      this.error = err.message;
    }
  }

  private async render(): Promise<void> {
    await this.display.render();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
