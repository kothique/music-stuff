import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { TabsService } from '../tabs.service';

@Component({
  selector: 'app-right-sidenav',
  template: `
    <app-right-sidenav-dumb
      *ngIf="(activeTab$ | async)"
      [docLink]="(activeTab$ | async)?.state.docLink">
    </app-right-sidenav-dumb>
  `
})
export class RightSidenavComponent implements OnInit {
  constructor(
    private tabsService: TabsService
  ) { }

  activeTab$ = this.tabsService.activeTab$;

  showPlugins$ = this.tabsService.activeTab$
    .pipe(map(tab => tab && (tab.state.type === 'normal' || tab.state.type === 'editing')));

  ngOnInit(): void { }
}
