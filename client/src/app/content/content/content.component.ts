import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { isNil } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DocumentService } from '../document.service';
import { Tab } from '../tab';
import { TabsService } from '../tabs.service';

@Component({
  selector: 'app-content',
  template: `
    <app-content-dumb
      *ngIf="(tabs$ | async).length"
      [activeTab]="activeTab$ | async"
      [activeIndex]="activeIndex$ | async"
      [tabs]="tabs$ | async"
      [wide]="wide"
      [drawerOpened]="drawerOpened"
      [rightDrawerOpened]="rightDrawerOpened"
      [shrinkTabs]="isSmall$ | async"
      (open)="onTabOpen($event)"
      (edit)="onTabEdit($event)"
      (save)="onTabSave($event)"
      (delete)="onTabDelete($event)"
      (cancel)="onTabCancel($event)"
      (close)="onTabClose($event)">
    </app-content-dumb>
  `
})
export class ContentComponent implements OnInit {
  constructor(
    private breakpointObserver: BreakpointObserver,
    private tabsService: TabsService,
    private documentService: DocumentService
  ) { }

  @Input() wide: boolean;
  @Input() drawerOpened: boolean;
  @Input() rightDrawerOpened: boolean;

  activeTab$: Observable<Tab|null> = this.tabsService.activeTab$;
  activeIndex$: Observable<number|null> = this.tabsService.activeIndex$;
  tabs$: Observable<Tab[]> = this.tabsService.tabs$;

  isSmall$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small])
    .pipe(map(result => result.matches));

  ngOnInit(): void { }

  onTabOpen(index: number): void {
    this.tabsService.openByIndex(index);
  }

  onTabEdit(index: number): void {
    this.tabsService.editByIndex(index);
  }

  onTabSave(index: number): void {
    this.tabsService.saveByIndex(index);
  }

  onTabDelete(index: number): void {
    const tab: Tab|null = this.tabsService.getTab(index);
    if (isNil(tab)) { return; }

    if (tab.state.type !== 'normal' && tab.state.type !== 'editing') {
      return;
    }

    this.documentService.delete(tab.state.docLink);
  }

  onTabCancel(index: number): void {
    this.tabsService.cancelByIndex(index);
  }

  onTabClose(index: number): void {
    const tab: Tab|null = this.tabsService.getTab(index);
    if (isNil(tab)) { return; }

    if (tab.state.type === 'normal' || tab.state.type === 'editing') {
      this.documentService.close(tab.state.docLink);
    }

    if (tab.state.type === 'creating') {
      this.tabsService.closeByIndex(index);
    }
  }
}
