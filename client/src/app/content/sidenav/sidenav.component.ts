import { Component, OnDestroy, OnInit } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SidenavMenuEntry } from '../../../shared/sidenav-menu-entry';
import { NotificationsService } from '../../notifications.service';
import { Document } from '../document';
import { DocumentLink } from '../document-link';
import { DocumentService } from '../document.service';

@Component({
  selector: 'app-sidenav',
  template: `
    <app-sidenav-dumb
      [entries]="entries">
    </app-sidenav-dumb>
  `
})
export class SidenavComponent implements OnInit, OnDestroy {
  constructor(
    private apollo: Apollo,
    private documentService: DocumentService,
    private notificationsService: NotificationsService
  ) { }

  entries: SidenavMenuEntry[] = [];

  private unsubscribe$ = new Subject<void>();
  private queryRef: QueryRef<{ allDocuments: Document[] }> =
    this.apollo.watchQuery<{ allDocuments: Document[] }>({
      query: gql`
        query AllDocuments {
          allDocuments {
            _id
            title
          }
        }
      `
    });

  ngOnInit(): void {
    this.queryRef.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ({ data }) => {
          this.entries = data.allDocuments.map(doc => ({
            title: doc.title,
            docLink: new DocumentLink('collection', doc._id)
          }));
        },
        error: (err) => {
          this.notificationsService.showError('Failed to load the document list.');
        }
      });

    this.documentService.create$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.queryRef.refetch());

    this.documentService.update$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.queryRef.refetch());

    this.documentService.delete$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.queryRef.refetch());
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
