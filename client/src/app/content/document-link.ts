import { DocumentProtocol } from '../../../../common/documents';

export class DocumentLink {
  constructor(
    public readonly protocol: DocumentProtocol,
    public readonly path: string
  ) { }

  toString(): string {
    return `${this.protocol}://${this.path}`;
  }
}
