import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ContentDumbComponent } from 'src/app/content/content-dumb/content-dumb.component';
import { ContentComponent } from 'src/app/content/content/content.component';
import { MaterialModule } from 'src/app/material/material.module';
import { PluginsModule } from '../plugins/plugins.module';
import { DocumentsModule } from './documents/documents.module';
import { FormCreateDocumentComponent } from './form-create-document/form-create-document.component';
import { FormEditDocumentComponent } from './form-edit-document/form-edit-document.component';
import { RightSidenavDumbComponent } from './right-sidenav-dumb/right-sidenav-dumb.component';
import { RightSidenavComponent } from './right-sidenav/right-sidenav.component';
import { SidenavDumbComponent } from './sidenav-dumb/sidenav-dumb.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { TabDumbComponent } from './tab-dumb/tab-dumb.component';
import { TabComponent } from './tab/tab.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    DocumentsModule,
    PluginsModule
  ],
  exports: [
    ContentComponent,
    SidenavComponent,
    RightSidenavComponent
  ],
  declarations: [
    ContentComponent,
    ContentDumbComponent,
    TabDumbComponent,
    TabComponent,
    SidenavDumbComponent,
    SidenavComponent,
    FormEditDocumentComponent,
    FormCreateDocumentComponent,
    RightSidenavComponent,
    RightSidenavDumbComponent
  ]
})
export class ContentModule { }
