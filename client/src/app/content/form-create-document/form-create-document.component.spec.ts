import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormCreateDocumentComponent } from './form-create-document.component';

describe('FormCreateDocumentComponent', () => {
  let component: FormCreateDocumentComponent;
  let fixture: ComponentFixture<FormCreateDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCreateDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCreateDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
