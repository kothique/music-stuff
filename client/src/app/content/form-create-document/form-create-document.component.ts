import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { DocumentFormat } from '../../../../../common/documents';
import { SelectOption } from '../../../shared/select-option';
import { Document, DocumentInput } from '../document';

@Component({
  selector: 'app-form-create-document',
  templateUrl: './form-create-document.component.html',
  styleUrls: ['./form-create-document.component.css']
})
export class FormCreateDocumentComponent implements OnInit, OnDestroy {
  constructor(
    private formBuilder: FormBuilder
  ) { }

  @Input() input: DocumentInput;
  @Output() inputChange = new EventEmitter<DocumentInput>();

  readonly formats: SelectOption<DocumentFormat>[] = [
    { text: 'HTML', value: 'html' },
    { text: 'VexTab', value: 'vextab' },
    { text: 'MusicXML', value: 'musicxml' }
  ];
  readonly defaultFormat = this.formats[0];

  form: FormGroup = this.formBuilder.group({
    format: [this.defaultFormat.value, Validators.required],
    title: ['', Validators.required],
    content: ''
  });

  private unsubscribe$ = new Subject<void>();

  ngOnInit(): void {
    this.form.valueChanges.subscribe(value => {
      this.inputChange.emit(value);
    });

    this.form.setValue({
      format: this.defaultFormat.value,
      title: '',
      content: ''
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
