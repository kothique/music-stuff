import { DocumentFormat } from '../../../../common/documents';

export interface Document {
  _id?: string;
  format?: DocumentFormat;
  title?: string;
  content?: string;
  updatedAt?: string;
  createdAt?: string;
}

export interface DocumentInput {
  format?: DocumentFormat;
  title?: string;
  content?: string;
}
