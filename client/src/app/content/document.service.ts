import { Injectable, OnDestroy } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { isNil } from 'lodash';
import { Subject } from 'rxjs';
import { DocumentFormat } from '../../../../common/documents';
import { NotificationsService } from '../notifications.service';
import { BufferService } from './buffer.service';
import { Document } from './document';
import { DocumentLink } from './document-link';

@Injectable({
  providedIn: 'root'
})
export class DocumentService implements OnDestroy {
  constructor(
    private apollo: Apollo,
    private bufferService: BufferService,
    private notificationsService: NotificationsService
  ) { }

  private createSubject = new Subject<DocumentLink>();
  readonly create$ = this.createSubject.asObservable();

  private openSubject = new Subject<DocumentLink>();
  readonly open$ = this.openSubject.asObservable();

  private updateSubject = new Subject<DocumentLink>();
  readonly update$ = this.updateSubject.asObservable();

  private deleteSubject = new Subject<DocumentLink>();
  readonly delete$ = this.deleteSubject.asObservable();

  private closeSubject = new Subject<DocumentLink>();
  readonly close$ = this.closeSubject.asObservable();

  private unsubscribe$ = new Subject<void>();

  async create(options: CreateOptions): Promise<DocumentLink|null> {
    try {
      const { data } = await this.apollo.mutate<{ createDocument: Document }>({
        mutation: gql`
          mutation CreateDocument($input: CreateDocumentInput!) {
            createDocument(input: $input) {
              _id
              format
              title
              content
            }
          }
        `,
        variables: {
          input: {
            format: options.format,
            title: options.title,
            content: options.content
          }
        }
      }).toPromise();

      const doc: Document = data.createDocument;

      const docLink = new DocumentLink('collection', data.createDocument._id);
      this.bufferService.add(docLink, doc);
      this.createSubject.next(docLink);

      return docLink;
    } catch (err) {
      this.notificationsService.showError(`Failed to save document: ${err.message}`);

      return null;
    }
  }

  async open(docLink: DocumentLink): Promise<DocumentLink|null> {
    try {
      const { data } = await this.apollo.query<{ document: Document }>({
        query: gql`
          query Document($protocol: DocumentProtocol!, $path: String!) {
            document(protocol: $protocol, path: $path) {
              _id
              format
              title
              content
              pluginStates {
                metronome
              }
            }
          }
        `,
        variables: {
          protocol: docLink.protocol,
          path: docLink.path
        }
      }).toPromise();

      const doc = data.document;

      this.bufferService.add(docLink, doc);
      this.openSubject.next(docLink);

      return docLink;
    } catch (err) {
      this.notificationsService.showError(`Failed to open a document: ${err.message}`);

      return null;
    }
  }

  async update(docLink: DocumentLink, options: SaveOptions): Promise<DocumentLink|null> {
    if (isNil(this.bufferService.get(docLink))) { return; }

    try {
      const { data } = await this.apollo.mutate<{ updateDocument: Document }>({
        mutation: gql`
          mutation UpdateDocument($path: String!, $input: UpdateDocumentInput!) {
            updateDocument(path: $path, input: $input) {
              _id
              format
              title
              content
            }
          }
        `,
        variables: {
          path: docLink.path,
          input: {
            format: options.format,
            title: options.title,
            content: options.content
          }
        }
      }).toPromise();

      const doc: Document = data.updateDocument;

      this.bufferService.update(docLink, doc);
      this.updateSubject.next(docLink);

      return docLink;
    } catch (err) {
      this.notificationsService.showError(`Failed to save the document: ${err.message}`);

      return null;
    }
  }

  async delete(docLink: DocumentLink): Promise<boolean> {
    try {
      await this.apollo.mutate<void>({
        mutation: gql`
          mutation DeleteDocument($path: String!) {
            deleteDocument(path: $path)
          }
        `,
        variables: {
          path: docLink.path
        }
      }).toPromise();

      this.bufferService.remove(docLink);
      this.deleteSubject.next(docLink);

      return true;
    } catch (err) {
      this.notificationsService.showError(`Failed to delete the document: ${err.message}`);

      return false;
    }
  }

  async close(docLink: DocumentLink): Promise<void> {
    this.bufferService.remove(docLink);
    this.closeSubject.next(docLink);
  }

  ngOnDestroy(): void {
    this.openSubject.complete();
    this.closeSubject.complete();

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

interface CreateOptions {
  format: DocumentFormat;
  title: string;
  content: string;
}

interface SaveOptions {
  format?: DocumentFormat;
  title?: string;
  content?: string;
}
