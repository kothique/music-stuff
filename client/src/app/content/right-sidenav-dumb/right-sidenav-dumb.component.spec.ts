import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightSidenavDumbComponent } from './right-sidenav-dumb.component';

describe('RightSidenavDumbComponent', () => {
  let component: RightSidenavDumbComponent;
  let fixture: ComponentFixture<RightSidenavDumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightSidenavDumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightSidenavDumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
