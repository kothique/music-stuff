import { Component, Input, OnInit } from '@angular/core';
import { DocumentLink } from '../document-link';

@Component({
  selector: 'app-right-sidenav-dumb',
  templateUrl: './right-sidenav-dumb.component.html',
  styleUrls: ['./right-sidenav-dumb.component.css']
})
export class RightSidenavDumbComponent implements OnInit {
  constructor() { }

  @Input() docLink: DocumentLink;

  ngOnInit(): void { }
}
