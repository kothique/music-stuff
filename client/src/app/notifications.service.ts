import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  constructor(
    private snackBar: MatSnackBar
  ) { }

  showError(msg: string, duration: number = 8000) {
    this.snackBar.open(msg, 'OK', { duration });
  }
}
