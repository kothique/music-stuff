import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { KeepHtmlPipe } from './keep-html.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    KeepHtmlPipe
  ],
  declarations: [
    KeepHtmlPipe
  ]
})
export class PipesModule { }
