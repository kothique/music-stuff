import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  constructor(
    private snackbar: MatSnackBar
  ) { }

  handleHttpError = (err: Error): Observable<never> => {
    this.snackbar.open(`Error: ${err.message}`, 'OK');
    return throwError(err);
  }

  handleHttpErrorPromise = (err: Error): Promise<never> => {
    this.snackbar.open(`Error: ${err.message}`, 'OK');
    throw err;
  }
}
