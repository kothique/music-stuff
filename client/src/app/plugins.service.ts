import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { DocumentLink } from './content/document-link';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class PluginsService {
  constructor(
    private apollo: Apollo,
    private util: UtilService
  ) { }

  getState(name: string, docLink: DocumentLink): Promise<string|null> {
    return this.apollo.query<{ document: { pluginStates: Record<string, string> } }>({
      query: gql`
        query PluginState($protocol: String!, $path: String!) {
          document(protocol: $protocol, path: $path) {
            pluginStates {
              ${name}
            }
          }
        }
      `,
      variables: {
        protocol: docLink.protocol,
        path: docLink.path
      }
    }).toPromise()
      .catch(this.util.handleHttpErrorPromise)
      .then(result => result.data.document.pluginStates[name]);
  }

  updateState(name: string, docLink: DocumentLink, state: string, docId: string): Promise<void> {
    return this.apollo.mutate({
      mutation: gql`
        mutation UpdatePluginState($name: String!, $input: UpdatePluginStateInput!) {
          updatePluginState(name: $name, input: $input)
        }
      `,
      variables: {
        name,
        input: {
          protocol: docLink.protocol,
          path: docLink.path,
          state
        }
      }
    }).toPromise()
      .catch(this.util.handleHttpErrorPromise)
      .then(() => {
        const doc: { pluginStates: Record<string, string> } = this.apollo.getClient().readFragment({
          id: `Document:${docId}`,
          fragment: gql`
            fragment readPluginState on Document {
              pluginStates {
                ${name}
              }
            }
          `,
          variables: {
            name,
            protocol: docLink.protocol,
            path: docLink.path
          }
        });
        doc.pluginStates['metronome'] = state;

        this.apollo.getClient().writeFragment({
          id: `Document:${docId}`,
          fragment: gql`
            fragment writePluginState on Document {
              pluginStates {
                ${name}
              }
            }
          `,
          variables: {
            name,
            protocol: docLink.protocol,
            path: docLink.path
          },
          data: doc
        });

        return null;
      });
  }
}
