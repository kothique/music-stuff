export type DocumentFormat = 'html' | 'vextab' | 'musicxml';
export const DOCUMENT_FORMATS = ['html', 'vextab', 'musicxml'];

export type DocumentProtocol = 'internal' | 'collection';