export interface PluginDescription {
  name: string;
  version: [number, number, number];
}